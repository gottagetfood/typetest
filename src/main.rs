use terminal_size::{Width, Height, terminal_size};
use std::io;
use std::io::prelude::*;
use std::fs::File;
use std::string::String;
extern crate cursive;
use cursive::Cursive;
use cursive::views::TextView;
use std::char::from_u32;
use std::thread::sleep;
use std::time;

fn term_size() -> (u16, u16) {
    let term_size = terminal_size();
    if let Some((Width(w), Height(h))) = term_size {
        (w, h)
    } else {
        (0,0)
    }
}

fn words() -> io::Result<String> {
    let mut f = File::open("words.txt")?;
    let mut buffer = String::new();
    f.read_to_string(&mut buffer)?;
    Ok(buffer)
}

fn gen_canvas(height: u16, width: u16) -> Vec<String> {
    let mut linestring: String = String::new();
    for i in 0..width {
        linestring.push_str(" ");
    }
    let mut canvasvec: Vec<String> = Vec::new();
    for i in 0..height {
        canvasvec.push(linestring.clone());
    }
    canvasvec
}

fn vectorstring(vector: Vec<String>) -> String {
    let mut the_string = String::new();
    for line in vector {
        the_string.push_str(&line);
    	the_string.push_str("\n");
    }
    the_string
}

fn push_space(canvasvec: &mut Vec<String>) {
    for line in canvasvec {
        let mut charvec: Vec<_> = line.chars().collect();
        let veclen: usize = charvec.len();
        charvec.insert(0, from_u32(' ' as u32).unwrap());
//        charvec.truncate(veclen);
        line = charvec.iter().cloned().collect();
    }
}

fn main() {
    let mut wordstring = String::new();
    let term_size = term_size();
    let words = words();
    println!("{:?} {:?}", term_size.0, term_size.1);
    match words {
        Ok(v) => wordstring = v,
        Err(e) => println!("Error: {}", e),
    }
    let words_vec: Vec<_> = wordstring.split("\n").collect();
    let mut siv = Cursive::ncurses();
    
    let textbox = siv.add_layer(TextView::new(vectorstring(gen_canvas((term_size.1 as f32*0.85) as u16, (term_size.0 as f32*0.90) as u16))));
    Cursive::run(&mut siv);
    let mut last_vec: Vec<_> = gen_canvas((term_size.1 as f32*0.85) as u16, (term_size.0 as f32*0.90) as u16);
    loop {
        push_space(&mut last_vec);
        sleep(time::Duration::from_millis(1000))
    }
}


